var userInput;
var inputValid;

// function to generate an array with numbers from 1 to userInput as individual items in the array numArray
var generateNumList = function (input){
  var numArray = [];
  for (var i = 1; i <= input; i++) {
    numArray.push(i);
  }
  return numArray;
};

// function to replace numbers in an array that are multiples of 3, 5, and 15 with text
var replaceNums = function (input){
  for (var i = 0; i < input.length; i++) {
    if (input[i] % 3 === 0 && input[i] % 15 !== 0) {
      input.splice(i, 1, "ping");
    } else if (input[i] % 5 === 0 && input[i] % 15 !== 0) {
      input.splice(i, 1, "pong");
    } else if (input[i] % 15 === 0 ) {
      input.splice(i, 1, "pingpong");
    }
  }
  return input;
};

// function to convert items in an array to list items in a ul
var arrayToUL = function (input){
  var html = "<ul>";
  for (var i = 0; i < input.length; i++) {
    html = html + "<li>" + input[i] + "</li>";
  }
  html = html + "</ul>";
  return html;
};

//function to check user input
var checkInput = function (input, target){

  // Check to see if input is a number
  if (!input){
    $(".help-block").remove();
    $("#" + target).parent().append('<span class="help-block">Please enter a number.</span>')
    $("#" + target).parent().addClass("has-error");
    inputValid = false;

    // Check to see if input is less than a maximum value
  } else if (input > 99999) {
    $(".help-block").remove();
    $("#" + target).parent().append('<span class="help-block">This is why we can\'t have nice things. Please enter a number less than 100,000.</span>')
    $("#" + target).parent().addClass("has-error");
    inputValid = false;

    // If input is validated
  } else {
    $("#" + target).parent().removeClass("has-error");
    $(".help-block").remove();
    inputValid = true;
  }
};

//captures user input upon click and appends ouput to page
$("#your-number-button").click(function() {

  // capture user input
  userInput = parseInt($("#your-number-input").val());

  // validate user input
  checkInput(userInput, "your-number-input");

  // if user input input is valid, produce results
  if (inputValid) {
    $(".output").html(arrayToUL(replaceNums(generateNumList(userInput))));
  } else {
    $(".output").empty();
  }

});
